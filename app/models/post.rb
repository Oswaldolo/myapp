class Post < ActiveRecord::Base
	validates_presence_of :title
	validates_length_of :title, :in=> 5..30, :message=>"Titulo invalido"
	validates_presence_of :body
	validates :body, length: { minimum: 10, message: "El texto del post es demasiado corto" }
	validates :body, length: { maximum: 500, message: "El texto del post es demasiado largo" }
	has_many :comments, dependent: :destroy
	validates_presence_of :title
	validates_presence_of :body
	belongs_to :users
end

